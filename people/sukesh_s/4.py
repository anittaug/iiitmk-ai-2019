# Material Questions

# - https://gitlab.com/gitcourses/iiitmk-ai-2019/merge_requests/339/diffs
#
# - which college was this recorded in?
# - who was the old man?
# - where did the speaker say she worked?
# - what color did one of the speakers dye her hair?

# - please write the program to generate random data?
# - what does backpropagation solve?
# - can you always solve small circuit search?
# - normally how much time do you need to sort something? Give big O notation
# - what is a stochastic environment?
# - name two things which are always present in reinforcement learning?
# - who gives the reward in the reinforcement setting?
# - what do you mean by corollary?
# - what reward does real life give?
# - why does reinforcement learning work better when you remove random actions from it?
# - name one class of reinforcement learning algorithms
# - name another class of reinforcement learning algorithms
# - what makes reinforcement learning systems data efficient?

# - what is the difference between meta learning / learning?
# - what is meta examinations?
# - what is meta programming?
# - what is meta programming?

# - what is a sparse reward?
# - What kind of reward does college life give us?
# - how useful is a map of scale 1:1?
# - why don't simulations match reality?
# - how can we turn this into a meta learning problem?

# - training task = test task. Example?
# - Why is it difficult to be good at a job initially?
# - what is a political problem?

# that should be enough for now. You could go back and watch that video once
# more since it's really really rich with information
# ========================================
# Notes
#
# - Simple Reflex Agents
# - Model-Based Reflex Agents
# - Goal-Based Agents
# - Utility-Based Agents
# - https://www.geeksforgeeks.org/agents-artificial-intelligence/
#
# ========================================
# 1. Copy an assignment file from this folder to the `people/<your name>` folder
# 2. Edit the `people/<your name>/<assignment number>.py` file and write your solution in it
# 3. Create a merge request to submit
# 4. Ensure that
#       - tests pass for your solution in the merge request
#       - you have setup your gitlab fork to auto-mirror the class
#
#
# =========================================
# Like the last assignment this time also you need to cross the road.
# This time however instead of a single lane road you will be crossing a 3 lane road.
# Last time you were given left/right road observations. This time you are given a function.
# YOU need to decide if you want to look left or right.
# Sounds more realistic right? Since you can't look left AND right at the same time physically

# Now, if you look left and then right, some TIME has passed between the two
# observations. So your observations also become "stale" or "out of date"
# It's up to you how to fix this problem.
# each time you "look", time advances by one unit.

# Traffic direction is still right to left, BUT the vehicles don't move at the same speed now.
# Some are fast, some are slow.

# just like last time, you still need to return True/False depending on if you
# should cross right now or not

# import re


# def joins(s):
#    return ["".join(["n" if pos is None else pos for pos in i]) for i in s]


# def splits(k1):
#    k2 = []
#    for j in k1:
#        k2.append([j[i : i + 3] for i in range(0, len(j), 3)])
#    return k2
#
#
# def speed(look):
#    s = [[11, 12, 13], [21, 22, 23], [31, 32, 33]]
#    x = 0
#    r1 = look("right")
#    r1 = joins(r1)
#    while True:
#        r2 = look("right")
#        r2 = joins(r2)
#        r1 = splits(r1)
#        sk = [11, 12, 13]
#        for i in range(3):
#            sc = re.search(
#                str("".join(reversed(r1[i][-1]))), str("".join(reversed(r2[i])))
#            ).span()[0]
#            if ("".join(reversed(r2[i])))[sc + 3 : sc + 6] == r1[i][-2][::-1]:
#                sk[i] = sc
#        s[2] = sk
#        if s[0] == s[2]:
#      x+=1

#    if x==1000:
#            break
#        else:
#            for i in range(2):
#                s[i] = s[i + 1]
#            s[2] = [31, 32, 33]
#            r1 = r2
#    return s[2]


def assignment_4(look) -> bool:
    # sp = speed(look)
    #  print("checking")
    #  print(sp[2])
    sp = [1, 2, 3]
    while True:
        right = look("right")
        t = 0
        for i in range(3):
            if right[i][sp[i] * i] is None:
                t += 1
        #        for i in range(2, -1, -1):
        #            for j in range(len(right[i])):
        #                if right[i][j] is not None:
        #                    if j > ((3 - i) * sp[i]):
        #                        t += 1
        #                        break
        #                    else:
        #                        break
        if t == 3:
            break
    return True
